const shortid = require("shortid");
var express = require("express");
var app = express();
const bodyParser = require("body-parser");
const Utils = require("./utils");
app.use(bodyParser.json());

function serve() {
  app.get("/ohms/:id/status", async (req, res) => {
    const trackingId = req.params.id;
    const ohm = await Utils.getOhmByTrackingId(trackingId);
    const status = ohm.status;
    res.send(status);
  });

  app.post("/ohms/:id/status", async (req, res) => {
    const trackingId = req.params.id;
    const newStatus = req.body.newStatus;
    const comment = req.body.comment;
    const availableStatuses = ["DELIVERED", "REFUSED"];
    if (!availableStatuses.includes(newStatus)) {
      return res.send({
        success: false,
        error:
          "newStatus must be one of the following:" +
          availableStatuses.join(","),
      });
    }
    const ohm = await Utils.updateOhmStatusByTrackingId(
      trackingId,
      newStatus,
      comment
    );
    res.send({
      ohm,
      success: true,
    });
  });

  app.post("/ohms/:id/statusTransition", async (req, res) => {
    const trackingId = req.params.id;
    const newStatus = req.body.newStatus;
    const statusesList = [
      "CREATED",
      "PREPARING",
      "READY",
      "IN_DELIVERY",
      "DELIVERED",
      "REFUSED",
    ];
    const ohm = await Utils.getOhmByTrackingId(trackingId);
    const currentStatus = ohm.status;
    const currentIndex = statusesList.findIndex((x) => x === currentStatus);
    const newIndex = statusesList.findIndex((x) => x === newStatus);
    const canTransite = () => {
      if (currentIndex + 1 === newIndex) return true;
      if (
        (currentStatus === "IN_DELIVERY" && newStatus === "DELIVERED") ||
        newStatus === "REFUSED"
      )
        return true;
      return false;
    };
    if (canTransite()) {
      const ohm = await Utils.updateOhmStatusByTrackingId(
        trackingId,
        newStatus,
      );
      res.send({
        ohm,
        success: true,
      });
    } else {
      return res.send({
        success: false,
      });
    }
  });

  app.listen(3000, () => console.log("listening on port 3000"));
}

serve();
